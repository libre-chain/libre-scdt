# Libre Contract Development Toolkit (CDT)

# Install 
Make sure you have docker installed locally (and can do a simple hello world), then run:

`docker-compose up --build`

Once the docker is running, open a new terminal + copy over your entire smart contract directory.

`cp -pvr $CONTRACT_SRC ./contracts/`

Then enter the docker and cd into the directory you copied, try to build following the README of that smart contract repo.

This is usually something like this:

```
cd <contract_dir>
mkdir build
cd build
rm -f *
cmake ..
make
```